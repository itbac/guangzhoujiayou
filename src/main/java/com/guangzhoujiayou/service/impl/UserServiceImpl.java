package com.guangzhoujiayou.service.impl;

import com.guangzhoujiayou.bean.User;
import com.guangzhoujiayou.dao.UserRepository;
import com.guangzhoujiayou.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

/**
 * 用户服务
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private UserRepository userRepository;
    @Override
    public User save(User user) {
        return mongoTemplate.save(user);
    }

    @Override
    public User findOneByUnionid(String unionid) {
        return userRepository.findUserByUnionid(unionid);
    }

    @Override
    public User findOneById(Long id) {
        return mongoTemplate.findById(id, User.class);
    }

}
