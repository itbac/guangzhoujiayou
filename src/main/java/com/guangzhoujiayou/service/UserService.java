package com.guangzhoujiayou.service;

import com.guangzhoujiayou.bean.User;


public interface UserService {
    //新增 或 修改
    User save(User user);
    //根据微信标识，查找用户
    User findOneByUnionid(String unionid);
    //根据用户ID，查询用户信息
    User findOneById(Long id);

}
