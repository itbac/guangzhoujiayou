package com.guangzhoujiayou.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;


public class DateUtil {

    // 时间类型转换
    public static Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
