package com.guangzhoujiayou.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;


public class GsonUtil {

    private static Gson gson = new GsonBuilder().create();

    public static <T> T fromJson(String json, Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }
    public static <T> T fromJson(JsonElement json, Class<T> classOfT){
        return gson.fromJson(json, classOfT);
    }
    public static String toJson(Object src) {
        return gson.toJson(src);
    }
    public static JsonElement toJsonTree(Object src) {
        return gson.toJsonTree(src);
    }

}
