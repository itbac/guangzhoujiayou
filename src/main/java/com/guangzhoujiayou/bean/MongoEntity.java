package com.guangzhoujiayou.bean;


import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * mongoDB 数据库实体父类
 * 实现自增主键，设置创建时间，更新时间。
 */
@Data
@Accessors(chain = true) //链式编程
public abstract class MongoEntity implements Serializable{

    private static final long serialVersionUID = 5470149862478691635L;

    private long id;

    private String createDate;

    private String updateDate;

    private String createTime;

    private String updateTime;


}
