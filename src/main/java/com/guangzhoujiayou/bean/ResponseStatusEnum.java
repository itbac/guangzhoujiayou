package com.guangzhoujiayou.bean;



public enum ResponseStatusEnum {
    r1(0, "操作成功"),
    r2(1, "操作失败"),
    r3(2, "操作异常"),
    r4(3, "账号失效"),
    r5(4, "账号未授权"),

    ;

    private int status;
    private String name;

    ResponseStatusEnum(int status, String name) {
        this.status = status;
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public ResponseStatusEnum setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getName() {
        return name;
    }

    public ResponseStatusEnum setName(String name) {
        this.name = name;
        return this;
    }
}
