package com.guangzhoujiayou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 统一返回包装类
 */
@Data //get、set 方法
@NoArgsConstructor //无参构造
@AllArgsConstructor //全参构造
@Accessors(chain = true) //链式编程
public class R implements Serializable {

    private static final long serialVersionUID = 925768396980038298L;
    /**
     * 状态码
     *
     * @see ResponseStatusEnum
     */
    private int status;
    //消息
    private String message;

    private Object data;
    //时间戳 单位：秒
    private long timestamp = System.currentTimeMillis() / 1000;

    //成功 success
    public static R success() {
        return new R().setStatus(ResponseStatusEnum.r1.getStatus());
    }

    //失败 fail
    public static R fail() {
        return new R().setStatus(ResponseStatusEnum.r2.getStatus());
    }

    //初始化
    public static R init() {
        return new R();
    }

}
