package com.guangzhoujiayou.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * 微信登录返回值
 */
@Data
public class WxSession implements Serializable {
    private static final long serialVersionUID = -3258387943642825178L;
    //用户在当前小程序的唯一标识
    private String openid;
    //会话秘钥
    private String session_key;
    //用户在开放平台的唯一标识符
    //微信开放平台帐号下的唯一标识
    private String unionid;
    //错误码
    private int errcode;
    //错误信息
    private String errmsg;
}
