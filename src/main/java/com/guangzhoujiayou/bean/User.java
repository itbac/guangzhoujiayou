package com.guangzhoujiayou.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 用户：居民、医生
 */
@EqualsAndHashCode(callSuper = true) //包括父类的属性
@Data //get、set 方法
@NoArgsConstructor //无参构造
@AllArgsConstructor //全参构造
@Accessors(chain = true) //链式编程
public class User extends MongoEntity{

    //微信唯一标识
    private String unionid;
    //用户昵称
    private String nickName;
    //头像地址
    private String avatarUrl;
    //性别 ：0未知，1男，2女。
    private int gender;
    //国家
    private String country;
    //省份
    private String province;
    //城市
    private String city;
    //语言:显示 country，province，city 所用的语言
    //en 英文、zh_CN 简体中文 、zh_TW 繁体中文
    private String language;
    //role 角色
    //居民resident 、医生 doctor
    private String role;
    //姓名
    private String userName;
    //年龄
    private int age;
    //电话
    private String telephone;
    //工作单位
    private String workUnit;
    //职业
    private String occupation;
    //现在住址
    private String address;
    //坐标
    private String coordinate;
    //备注
    private String remarks;






}
