package com.guangzhoujiayou.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;


@Configuration
@ConfigurationProperties(prefix = "wxconfig")
@Data
public class WxConfig {
    @Value("${wxconfig.appid}")
    private String appid;
    @Value("${wxconfig.secret}")
    private String secret;
    @PostConstruct
    public void init(){
        System.out.println(String.format
                ("WxConfig.init,appid:{%s},secret:{%s}", appid, secret));
    }
}
