package com.guangzhoujiayou;

import com.guangzhoujiayou.mongo.EnableJiaYouMongoDB;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 广州加油
 * 疫情防控公益项目
 * 提供新冠病毒疫苗预约信息收集
 */
@SpringBootApplication
@EnableJiaYouMongoDB
public class GuangZhouJiaYou {
    public static void main(String[] args) {
        SpringApplication.run(GuangZhouJiaYou.class, args);
    }
}
/**
 * 功能设计
 * 人物：居民、医生
 * 医生公开自己的： 医院名称、地址、坐标、医院电话、医生姓名、医生岗位、医生的医院工牌照、医生联系电话。
 * 帮助居民，预约新冠病毒疫苗，有序安排接种工作。
 *
 * 居民自愿报名
 *
 * 居民查看附近的医院,选择一个医院,把自己的个人信息，填报到系统。
 * 仅你选择的医生可以查看您的信息，为您安排预约新冠病毒疫苗预约。请保持手机号畅通。
 *
 * 可以填报自己的，也可以帮亲人填报。
 *
 */
