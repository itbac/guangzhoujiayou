package com.guangzhoujiayou.jwt;

import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
@ConditionalOnBean(JwtConfig.class) //当给定的bean存在，则实例化当前bean
public class JwtTokenUtil {

    @Autowired
    private JwtConfig jwtConfig;


    //生成token
    public String generateToken(String subject, Map<String, Object> map) throws Exception {
        return JWTHelper.generateToken(subject, map,
                RsaKeyHelper.generatePrivateKey(jwtConfig.getRsaSecret()), jwtConfig.getExpire());
    }
    //解析token
    public Claims getInfoFromToken(String token) throws Exception {
        return JWTHelper.getInfoFromToken(token,
                RsaKeyHelper.generatePublicKey(jwtConfig.getRsaSecret()));
    }

}
