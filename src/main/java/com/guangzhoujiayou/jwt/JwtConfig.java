package com.guangzhoujiayou.jwt;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;


@Configuration
@ConfigurationProperties(prefix = "jwt")
@Data
public class JwtConfig {
    //RSA 加密密码
    @Value("${jwt.rsaSecret}")
    private String rsaSecret;
    //过期时间
    @Value("${jwt.expire}")
    private int expire;

    @PostConstruct
    public void init(){
        System.out.println(String.format
                ("JwtConfig.init,rsaSecret:{%s},expire:{%s}", rsaSecret, expire));
    }
}
