package com.guangzhoujiayou.mongo;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 定制 mongoDB 线程池配置
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({JiaYouMongoDBConfig.class})
public @interface EnableJiaYouMongoDB {

}
