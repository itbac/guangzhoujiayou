package com.guangzhoujiayou.mongo;

import cn.hutool.core.lang.Snowflake;
import com.guangzhoujiayou.bean.MongoEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
//@ConditionalOnBean(MongoClientOptionProperties.class)
@Slf4j
public class MongoInsertEventListener extends AbstractMongoEventListener<MongoEntity> {

    @Autowired
    private Snowflake snowflake;


    private static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Override
    public void onBeforeConvert(BeforeConvertEvent<MongoEntity> event) {
        MongoEntity entity = event.getSource();
        // 判断 id 为空
        LocalDateTime now = LocalDateTime.now();
        String time = now.format(timeFormatter);
        String date = now.format(dateFormatter);
        if (StringUtils.isEmpty(entity.getCreateDate())) {
            //创建日期，时间
            entity.setCreateDate(date)
                    .setCreateTime(time)
                    .setUpdateDate(date)
                    .setUpdateTime(time);
        }
        if (entity.getId() == 0) {
            // 雪花算法
            long id = snowflake.nextId();
            // 设置主键
            entity.setId(id);
            log.debug("MongoInsertEventListener,新增数据,设置主键ID");
        } else {
            log.debug("MongoInsertEventListener,更新数据,设置更新时间");
            //更新日期，时间
            entity.setUpdateDate(date)
                    .setUpdateTime(time);
        }

    }


}
