package com.guangzhoujiayou.dao;

import com.guangzhoujiayou.bean.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * 用户表
 */
@Repository
public interface UserRepository extends MongoRepository<User, Long> {

    User findUserByUnionid(String unionid);

}
