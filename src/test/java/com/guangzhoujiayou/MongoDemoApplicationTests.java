package com.guangzhoujiayou;

import com.guangzhoujiayou.bean.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MongoDemoApplicationTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    //新增数据
    @Test
    public void test1() {
        //insert 不能更新
        //save 能更新
        User user = new User();
        user.setId(1399017361574596608L);
        user.setAge(28);
        user.setUserName("lisi3");
        User insert = mongoTemplate.save(user);
        System.out.println(">>>>>>" +insert.toString());
    }



}
